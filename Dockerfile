FROM alpine:3.16.0 as builder
RUN apk add --no-cache autoconf gcc libc-dev libpcap-dev make zlib-dev zlib-static
WORKDIR /build
COPY src .
ENV LDFLAGS="-static -s -O2"
RUN autoconf && autoheader && ./configure && make
FROM scratch
COPY --from=builder /build/darkstat /usr/sbin/
# Dummy /etc/passwd file with a single entry for nobody user (required by getpwnam)
COPY --chown=0:0 etc/passwd /etc/
ENTRYPOINT ["/usr/sbin/darkstat", "--no-daemon", "--user", "nobody"]
